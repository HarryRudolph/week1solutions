def returnNumber(inputNumber):
    try:
        return(int(inputNumber))
    
    except ValueError:
        print("Try a number.")
        return None
    

if __name__ == '__main__':
    usrInput = input("Enter a number: ")
    
    print(returnNumber(usrInput))
