def toCSV(inputString):
    newList = inputString.strip().split()

    for i in newList:
        i = i.strip()
    
    return newList

if __name__ == "__main__":
    inputString = "   the        quick brown fox jumps \n over the lazy dogs    "
    print("The initial String: "+inputString)
    print("The new list: " + str(toCSV(inputString)))
