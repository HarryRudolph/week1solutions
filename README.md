# Week 1 Exercises

1. Write a function that converts one of {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"} into a corresponding integer value. For example, the function should return 1 when it is given "one". The function should have one input argument and one return value. The inputargument should be a string and the return value should be an integer.

2. Write a function that returns the average value of input floating point values that are provided in a list. The function should have one input argument and one return value. The input argument should be the list and return value should be the average. If the input list is empty, then the function should return None.

3. Write a function that converts a string to an integer, by casting the string to an integer. If the string is not an integer, then the function should return None. The function should have one input argument and one return value. The input argument should be the string that is to be converted to an integer. The input argument should have the form "1242" or some other numeric value. The function should use try and except to catch the ValueError exception that is thrown if the string is not an integer. For example, if the function is provided with "1242sd" it should return None.

4. Write a function to compute the sample standard deviation of floating point input values that are provided in a list. The function should have one input argument and one return value. The input argument should be a list of input values and the return argument should be the sample standard deviation. The function should return None when the input list contains zero or one element. If the input list contains two or more values, then it should return the sample standard deviation.

5. An input text string contains newline characters and additional spaces, together withcommas. Write a function that:
   - Removes the newline characters.
   - Splits the string using the comma character.
   - Removes any leading and trailing white space from the resulting substrings.
   - Returns the resulting list of values.
Create a test text string to demonstrate that the function works as expected. The function should have one input argument and one return value. The input argument should be in the input string, whereas the return value should be the resulting list.
