def returnNumber(inputNumber):

    options = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", ]
    
    if inputNumber.lower() in options:
        return (options.index(inputNumber.lower()) + 1)

    else:
        print("Please enter your number correctly")
    
    
if __name__ == '__main__':
    number = input("Number between one and ten: ")

    print(returnNumber(number))
