def averageFloat(inputList):
	if len(inputList) == 0:
		return None
	else:
		total = 0
		for i in inputList:
			total += i

		average = total/len(inputList)
		return average



if __name__ == '__main__':
	inputList = [3.23, 56.6, 53.7]
	print(averageFloat(inputList))
