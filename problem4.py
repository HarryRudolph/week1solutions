import math

def getAverage(nums):
    total = 0
    for i in nums:
        total = total + i
    return(total/len(nums))



def standardDev(inputList):
    if len(inputList) <= 1:
        return None
    
    average = getAverage(inputList)
    
    for i in range(len(inputList)):
        inputList[i] = inputList[i] - average
        inputList[i] = inputList[i]*inputList[i]
        
    return(math.sqrt(getAverage(inputList)))
    
if __name__ == "__main__":
    initialList = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    print(standardDev(initialList))
